#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <assert.h>
#define CTX 0xBABE


typedef int (func_t)(int);
enum state_e{
CTX_READY, CTX_TERMINATED, CTX_ACTIVATED};

struct ctx_s {

	void* ctx_esp;/* pointeur qui pointe sur le top de la pile */
	void* ctx_ebp;/* pointeur qui pointe sur la base de la pile */
	unsigned int ctx;/*  */
	func_t * f;
	void * arg;
	enum state_e status;
	unsigned char* stack;
};

int init_ctx(struct ctx_s * pctx, int stack_size, func_t f, void* args){
	pctx ->stack = malloc(stack_size);
	if(! pctx -> stack)
	return 0;
	pctx->ctx_ebp= &pctx->stack[stack_size-4];
	pctx->ctx_esp= pctx->ctx_ebp;
	pctx->f = f;
	pctx->arg = args;
	pctx->status = CTX_READY;
	pctx->ctx = CTX;
	return 1;
	}





volatile int try(struct ctx_s *pctx, func_t *f, int arg){
	
		asm("movl %%esp, %0\n" 
			:"=r"(pctx->ctx_esp));
			
		asm("movl %%ebp, %0\n" 
			:"=r"(pctx->ctx_ebp));
			
		pctx-> ctx = CTX;
	
	return (*f)(arg);
}


volatile int throw(struct ctx_s *pctx, int r){
	static int ret = 0;
	ret = r;
	
	assert(pctx->ctx == CTX);
	
		asm("movl %0, %%esp\n" 
			::"r"(pctx->ctx_esp):);
			
		asm("movl %0, %%ebp\n" 
			::"r"(pctx->ctx_ebp):);

	return ret;
}


void switch_to_ctx(struct ctx_s * newctx){
	static struct ctx_s * ctx_current =NULL;
	assert(newctx->ctx == CTX) || (newctx->status!=CTX_TERMINATED);
	if(ctx_current){
	asm("movl %% esp, %0":"=r"(ctx_current->ctx_esp)::);
	asm("movl %% ebp, %0":"=r"(ctx_current->ctx_ebp)::);}
	ctx_current= newctx;
	asm("movl %0 , %%esp"::"r"(ctx_current->ctx_esp):);
	asm("movl %0 , %%ebp"::"r"(ctx_current->ctx_ebp):);
	if(ctx_current-> status==CTX_READY){
	ctx_current -> status= CTX_ACTIVATED;
	ctx_current->f(ctx_current->arg);
	ctx_current->status= CTX_TERMINATED;
	exit(0);
	}
	return;
}

struct ctx_s ctx_ping;
struct ctx_s ctx_pong;

void f_ping(void *arg);
void f_pong(void *arg);

int main(int argc, char *argv[])
{
	init_ctx(&ctx_ping, 16384, f_ping, NULL);
	init_ctx(&ctx_pong, 16384, f_pong, NULL);
	switch_to_ctx(&ctx_ping);
	exit(EXIT_SUCCESS);
}

void f_ping(void *args){
	while(1) {
	printf("A") ;
	switch_to_ctx(&ctx_pong);
	printf("B") ;
	switch_to_ctx(&ctx_pong);
	printf("C") ;
	switch_to_ctx(&ctx_pong);
	}
}

void f_pong(void *args){
	while(1) {
	printf("1") ;
	switch_to_ctx(&ctx_ping);
	printf("2") ;
	switch_to_ctx(&ctx_ping);
	}
}
